package com.hoang.etc;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    ProgressBar progressBarLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        findViewById(R.id.btn_login).setOnClickListener(this);
        progressBarLogin = (ProgressBar) findViewById(R.id.loading_bar);
        progressBarLogin.setIndeterminate(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login : {
                Drawable drawable = v.getBackground();
                if(v.getBackground().getLevel() == 0){
                    progressBarLogin.setVisibility(View.VISIBLE);
                    ((Button)v).setTextColor(getResources().getColor(R.color.colorTextBtnSelect));
                    ((Button)v).setText("Đang đăng nhập");
                    drawable.setLevel(1);
                }else {
                    progressBarLogin.setVisibility(View.INVISIBLE);
                    ((Button)v).setTextColor(getResources().getColor(R.color.colorTextBtn));
                    ((Button)v).setText("Đăng nhập");
                    drawable.setLevel(0);
                }
            }
        }
    }
}
